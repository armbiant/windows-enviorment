$ErrorActionPreference = "Stop"

$wenv_root="C:/wenv"

if (-Not (Test-Path $wenv_root)) {
    New-Item $wenv_root -ItemType Directory | Out-Null
}
Push-Location -Path $wenv_root

if (Test-Path wenv-master) {
    Remove-item -Recurse -Force wenv-master
}

$url="https://gitlab.com/Linaro/windowsonarm/wenv/-/archive/master/wenv-master.zip"
Invoke-WebRequest -Uri $url -OutFile wenv.zip

Expand-Archive -Path wenv.zip -DestinationPath .
$script="$wenv_root/wenv-master/wenv.ps1"
echo "you can now call: $script YOUR_ARCH"
