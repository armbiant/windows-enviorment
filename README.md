# wenv

Create a windows dev environment from scratch

### Features

- dependencies are automatically download/installed in a specific folder (C:/wenv)
- downloads are cached when possible
- allow creation of x64/arm64 environment (both available at the same time)

Note: Visual Studio/Windows SDK/Windows Driver Kit/Dotnet are installed system
wide (and shared between environments)

### Setup

You can either clone this repository (if you have git), or run this command from
powershell **admin** prompt:

```
Set-ExecutionPolicy -ExecutionPolicy Unrestricted -Force; iex ((New-Object System.Net.WebClient).DownloadString('https://gitlab.com/Linaro/windowsonarm/wenv/-/raw/master/download.ps1'))
```

From there, you can call wenv with:

```
> C:/wenv/wenv-master/wenv.ps1
```

### Create an environment

wenv requires to be called from an **admin** prompt. It will check that.

```
# call without any parameter to show help
> ./wenv.ps1
usage: arch

arch: arm64|x64

> ./wenv.ps1 arm64 # from powershell
> ./wenv.bat arm64 # from cmd
> ./wenv arm64 # from WSL
```

**Note**: If you call this command again, the whole environment is destroyed,
and then created again.

You can change wenv location by setting WENV_PATH

```
> $env:WENV_PATH="C:/other_wenv"
> ./wenv.ps1 arm64
```

### Activate environment

Once created, an activate.bat is available.

```
# mix msys and windows command (which cl)
> cmd.exe /c "C:/wenv/arm64/activate.bat" which cl.exe
/c/Program Files (x86)/Microsoft Visual Studio/.../cl.exe

# spawn powershell
> cmd.exe /c "C:/wenv/arm64/activate.bat" powershell

# prefer cmd?
> cmd.exe /c "C:/wenv/arm64/activate.bat" cmd

# bash lover?
> cmd.exe /c "C:/wenv/arm64/activate.bat" bash

# just wanna load env in current cmd console?
> call "C:/wenv/arm64/activate.bat"
```

### Environment content

- 7-Zip
- Apache Ant
- Azure CLI
- CMake
- Dependencies (DLL analysis tool)
- Dotnet 3.5 + 6 (x86, x64, arm64) + core 3.1 (x86, x64)
- gitlab-runner
- LLVM
- MSYS2
- Nssm (Non Sucking Service Manager)
- nuget
- OpenJDK (Microsoft build)
- Python
- Qt
- Rust
- Strawberry Perl
- vcpkg
- Registry keys to fix settings
- Visual Studio Community 2019 (C++)
- Visual Studio Community 2022 (C++, dotnet) (used by default)
- Visual Studio Preview 2022 (C++
- Visual Studio redistributables
- Windows SDK
- Windows Driver Kit (WDK)

### Switch between Visual Studio versions

By default, latest stable Visual Studio version is enabled (using
vcvarsall.bat). This script is added to PATH so you can conveniently call it.

If you want to switch to another version, older or preview, you
must follow those steps:

```
> cmd.exe /c "C:/wenv/arm64/activate.bat" cmd.exe
# opens up cmd.exe
> which cl.exe
/c/Program Files/Microsoft Visual Studio/2022/Community/VC/Tools/MSVC/14.31.31103/bin/HostX64/ARM64/cl.exe

> call vcvarsall.bat /clean_env
> which cl.exe
which: no cl.exe in ...
# activate preview
> call "C:\Program Files\Microsoft Visual Studio\2022\Preview\VC\Auxiliary\Build\vcvarsall.bat" x64_arm64
> which cl.exe
/c/Program Files/Microsoft Visual Studio/2022/Preview/VC/Tools/MSVC/14.33.31629/bin/HostX64/ARM64/cl.exe
# preview is now correctly enabled!
```

Note: If you forget to call /clean_env when switching to preview version, it
won't result in any error. However, selected compiler will still be stable. So,
always ```call vcvarsall.bat /clean_env``` first.

Note: If you added some folders to PATH, vcvarsall.bat clean will remove them.
To ensure they stay on PATH, you can use ```set __VSCMD_PREINIT_PATH=%PATH%```.
This will force current PATH to be kept. However, it's still better to construct
your whole PATH in a variable, and set it accordingly after calling
vcvarsall.bat.
