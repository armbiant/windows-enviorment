#Requires -RunAsAdministrator

$ErrorActionPreference = "Stop"

if (-Not (Test-Path env:WENV_PATH))
{
    $env:WENV_PATH="C:/wenv"
}

if (-Not (Test-Path -Path $env:WENV_PATH))
{
    echo "create wenv folder"
    New-Item $env:WENV_PATH -ItemType Directory | Out-Null
}

$msys_path="$env:WENV_PATH/bootstrap_msys2/"
if (-Not (Test-Path $msys_path/installed))
{
    echo "need to install msys2 (only done once)"
    if (Test-Path $msys_path)
    {
        echo "cleanup previous msys_path: $msys_path"
        Remove-item -Recurse -Force $msys_path
    }
    New-Item $msys_path -ItemType Directory | Out-Null
    Push-Location -Path $msys_path
    $msys2_url="https://github.com/msys2/msys2-installer/releases/download/2022-12-16/msys2-base-x86_64-20221216.sfx.exe"
    # speed up invoke webrequest
    $ProgressPreference = 'SilentlyContinue'
    echo "Download msys2 from $msys2_url"
    Invoke-WebRequest -Uri $msys2_url -OutFile msys2_setup.exe
    echo "install msys2"
    Start-Process ./msys2_setup.exe -Wait
    New-Item $msys_path/installed -ItemType File | Out-Null
    Pop-Location
}

Push-Location -Path $PSScriptRoot
$wenv_script="wenv"
if (-Not (Test-Path -PathType Leaf $wenv_script))
{
    echo "Can't find script $PSScriptRoot\$wenv_script"
    exit 1
}

$env:PATH="$msys_path/msys64/usr/bin;$env:PATH"
$unix_wenv_path=cygpath -u $env:WENV_PATH
bash.exe -c "env WENV_PATH=`"$unix_wenv_path`" ./$wenv_script $args"
Exit $LASTEXITCODE
